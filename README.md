## Forecasting temperature time series using a machine learning model

The idea of the project was to use a machine learning model (random forest) to predict future weather (temperature) **in Tallinn, Estonia**. Essentially, it is a tool for time series forecasting.

The project consists of 5 interconnected parts:

* downloading weather data for Tallinn from a public database (IEM ASOS), which holds historical weather parameter time series for a bunch of airports around the world,
* parsing the data and eventually converting the training features and prediction parameter to numpy arrays,
* training the model,
* using the model to generate a temperature forecast (5 days ahead),
* saving the forecast as a png file.

The length of the training dataset is from the start of 2016 up to midnight of the running day.

The training features (variables) of the model are:

* temperature 5 hours ago
* temperature 4 hours ago
* temperature 3 hours ago
* temperature 2 hours ago
* temperature 1 hour ago
* number of month
* number of day
* number of hour

The most important variable (as expected) is temperature 1 hour ago.

**NB!** The training data is in UTC timezone and so is the forecast.

An example of what the output should look like is presented the file *Forecast_2017-12-28_16:02.png*.

## Requirements
* Python 3
* numpy
* pandas
* sklearn
* matplotlib
* unittest

## How to run it?
`python3 run_forecast.py`

## How to run the test?
`python3 -m unittest test.py`