import numpy as np
import pandas as pd
import os
from datetime import datetime


def get_csv_filename():
	'''Get the filename of the downloaded csv file with the weather data.'''
	dir_path = os.path.dirname(os.path.realpath(__file__))

	csv_files = [file for file in os.listdir(dir_path) if "csv" in file]

	# returns the last (most recent) file
	return csv_files[-1]


def load_weather_data(fname, header_row_number=5):
	''' Load weather data into a dataframe. '''
	data = pd.read_csv(fname, header=header_row_number)

	return data


def thin_out_dataframe(df):
	''' Get each second row from the dataframe, as the sampling period is 0.5h. '''
	thinned_df = df.iloc[::2, :]

	return thinned_df


def convert_time_axis_to_datetime(time_axis):
	''' Convert string time axis to a list of datetime objects. '''
	time_dt = list(map(lambda d: datetime.strptime(d, "%Y-%m-%d %H:%M"), time_axis))

	return time_dt


def get_hour_feature(time_dt):
	''' Get hour from the datetime objects. '''
	hour = list(map(lambda t: t.hour, time_dt))

	return hour


def get_day_feature(time_dt):
	''' Get day from the datetime objects. '''
	day = list(map(lambda t: t.day, time_dt))

	return day


def get_month_feature(time_dt):
	''' Get month from the datetime objects. '''
	month = list(map(lambda t: t.month, time_dt))

	return month


def fahrenheit_to_celsius(temperature_data):
	''' Convert from Fahrenheit to Celsius. '''
	celsius_temp = (temperature_data - 32) * 5/9

	return celsius_temp


def build_training_df(df, month, day, hour, celsius_temp):
	''' Build a single pandas dataframe of the training data. '''
	training_df = df[['valid']]
	training_df.loc[:,'month'] = month
	training_df.loc[:,'day'] = day
	training_df.loc[:,'hour'] = hour
	training_df.loc[:,'celsius_temp'] = celsius_temp

	return training_df


def add_timeseries_lag_features(df, col_name, nr_of_hours):
	''' Add the lagged time series as variables (features). '''
	all_column_names = list(df.columns)
	all_column_names.reverse()

	# get the series to be lagged
	temp_df = df.loc[:,col_name]
	# lag time series
	df = pd.concat([temp_df.shift(1), df], axis=1)
	current_column_name = col_name + "-" + str(1) 
	all_column_names.append(current_column_name)

	for i in range(nr_of_hours - 1):
		current_column_name = col_name + "-" + str(i + 2) 
		df = pd.concat([temp_df.shift(i + 2), df], axis=1)
		all_column_names.append(current_column_name)

	all_column_names.reverse()
	df.columns = all_column_names

	# remove NaN's at the start of the dataframe:
	df = df[nr_of_hours:]

	return df


def subset_train_df_to_x_y(df, column_to_be_predicted):
	''' Subset the dataframe to X and Y and convert to numpy arrays. '''
	all_columns = list(df.keys())
	train_columns = [col for col in all_columns if col not in [column_to_be_predicted, 'valid']]

	X = df.ix[:,train_columns]
	Y = df.ix[:,[column_to_be_predicted]]

	X = np.array(X)
	Y = np.ravel(np.array(Y))

	return X, Y
