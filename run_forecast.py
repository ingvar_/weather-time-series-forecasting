import forecast
import forecast_plot as fp
import iem_scraper
import model
import utils

# ----------------------------
# This script holds the main workflow of downloading weather data, generating a training set, training a random forest,
# making a temperature forecast using the trained model and plotting+saving the file to a png.
# ----------------------------

print("Downloading weather data for training....")
iem_scraper.main()
csv_fname = utils.get_csv_filename()

print("Downloaded.")
print("Processing data...\n\n\n")

data = utils.load_weather_data(csv_fname, header_row_number=5)
data = utils.thin_out_dataframe(data)
time_dt = utils.convert_time_axis_to_datetime(data['valid'])
hour = utils.get_hour_feature(time_dt)
day = utils.get_day_feature(time_dt)
month = utils.get_month_feature(time_dt)

celsius_temp = utils.fahrenheit_to_celsius(data['tmpf'])

print("Generating training set...")
training_df = utils.build_training_df(data, month, day, hour, celsius_temp)
training_df = utils.add_timeseries_lag_features(training_df, 'celsius_temp', 5)

X, Y = utils.subset_train_df_to_x_y(training_df, 'celsius_temp')

print("\n\n\nTraining random forest...")
rf = model.train_random_forest(X, Y)

print("Model trained.")

print("Doing a rolling forecast for the next 120 hours (5 days) starting from the last available timestep in the training data...")
print("NB!! Last timestep is usually the last hour of the previous day.")
times, Y_fcst = forecast.rolling_forecast(rf, training_df, 'celsius_temp', 120)

print("Done.")
print("Plotting and saving forecast...")
fname = fp.generate_filename()
fp.plot_and_save_forecast(times, Y_fcst, fname)

print("All done!")