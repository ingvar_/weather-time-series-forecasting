from pandas import concat
from sklearn.ensemble import RandomForestRegressor
from sklearn.datasets import make_blobs
from sklearn.model_selection import cross_val_score
from sklearn.externals import joblib


def train_random_forest(X_train, Y_train, n_trees=150):
	''' Train a random forest with the default of 150 trees. '''
	rf = RandomForestRegressor(n_estimators=n_trees, max_depth=None, min_samples_split=2, random_state=0)
	rf.fit(X_train, Y_train)
 
	return rf


def save_trained_rf(rf, fname="trained_model.pkl"):
	''' Save trained random forest to a pickle. Not used at the moment,
	as it doesn't take long to train the model. '''
	joblib.dump(rf, fname)

	return


def predict_using_trained_model(rf, X):
	''' Predict using a trained model and new variables. '''
	prediction = rf.predict(X)

	return prediction


