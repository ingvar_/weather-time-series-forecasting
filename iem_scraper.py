'''
Script that scrapes data from the IEM ASOS download service. 
Obtained from https://mesonet.agron.iastate.edu/request/download.phtml and modified.
'''
import json
import time
import datetime
import urllib.request
import os 


# Number of attempts to download data
MAX_ATTEMPTS = 6
SERVICE = "http://mesonet.agron.iastate.edu/cgi-bin/request/asos.py?"


def download_data(uri):
    '''Fetch the data from the IEM
    The IEM download service has some protections in place to keep the number
    of inbound requests in check.  This function implements an exponential
    backoff to keep individual downloads from erroring.
    '''
    attempt = 0
    while attempt < MAX_ATTEMPTS:
        try:
            data = urllib.request.urlopen(uri, timeout=300).read()
            if data is not None:
                return data
        except Exception as exp:
            print("download_data({}) failed with {}".format(uri, exp))
            time.sleep(5)
        attempt += 1

    print("Exhausted attempts to download, returning empty data")
    return ""


def main():
    '''Main method'''
    # timestamps in UTC to request data for
    # start from 2016-01-01
    startts=datetime.datetime(2016, 1, 1)
    # end today
    endts=datetime.datetime.today()

    service = SERVICE + "data=all&tz=Etc/UTC&format=comma&latlon=yes&"

    service += startts.strftime('year1=%Y&month1=%m&day1=%d&')
    service += endts.strftime('year2=%Y&month2=%m&day2=%d&')

    # EE - Estonia network
    network = 'EE__ASOS'

    # Get metadata
    sites = "EETN"
    sitename = "EETN"
    uri = '{}&station={}'.format(service, sitename) 
    print('Network: {} Downloading: {}'.format(network, sitename))

    data = download_data(uri)

    dir_path = os.path.dirname(os.path.realpath(__file__))
    outfn = '{}_{}_{}.csv'.format(sitename, startts.strftime("%Y%m%d%H%M"),
                                   endts.strftime("%Y%m%d%H%M"))

    full_fn = os.path.join(dir_path, outfn)

    with open(full_fn, 'wb') as out_file:
        out_file.write(data)


if __name__ == '__main__':
    main()