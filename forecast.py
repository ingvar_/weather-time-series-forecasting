import numpy as np
import utils
from datetime import datetime, timedelta
import model


def rolling_forecast(rf_model, train_df, column_to_be_predicted, nr_of_forecast_hours, nr_of_preceding_hours=5):
	'''
	Do a rolling forecast starting from the last available timestep in the training data.
	By rolling forecast it is meant that the model starts generating values starting from the last available timestep,
	without any further new information about "the future".
	'''
	# get last timestep of training data
	last_timestep_of_data = train_df.iloc[-1,:]

	# get datetime object of the last timestep
	last_available_time = last_timestep_of_data['valid']
	last_available_time = datetime.strptime(last_available_time, "%Y-%m-%d %H:%M")

	# get all forecast times of the 'soon-to-be-predicted' times
	all_forecast_times = [last_available_time]
	for i in range(nr_of_forecast_hours):
		all_forecast_times.append(all_forecast_times[i] + timedelta(hours=1))

	# get the dataframe of the starting point
	df_columns = [key for key in train_df.keys() if key not in ['valid', column_to_be_predicted]]

	X_rolling_prediction_tmp = np.array(last_timestep_of_data.ix[df_columns])
	Y_forecast = np.array(last_timestep_of_data.ix[column_to_be_predicted])
	Y_forecast = Y_forecast.reshape(1, -1)

	# reshape row into array of arrays
	X_rolling_prediction_tmp = X_rolling_prediction_tmp.reshape(1, -1)

	for i in range(nr_of_forecast_hours):
		# predict temperature timestep
		y_timestep_prediction_tmp = model.predict_using_trained_model(rf_model, X_rolling_prediction_tmp)
		# roll the values so that the last one can be replaced
		X_rollable = X_rolling_prediction_tmp[0, 0:nr_of_preceding_hours]
		X_rolling_prediction_tmp = np.roll(X_rollable, -1)
		# replace last temperature value of the lagged feature with predicted new
		X_rolling_prediction_tmp[-1] = y_timestep_prediction_tmp[0]
		# add month, day, hour
		X_rolling_prediction_tmp = np.append(X_rolling_prediction_tmp, all_forecast_times[i + 1].month)
		X_rolling_prediction_tmp = np.append(X_rolling_prediction_tmp, all_forecast_times[i + 1].day)
		X_rolling_prediction_tmp = np.append(X_rolling_prediction_tmp, all_forecast_times[i + 1].hour)
		# reshape row to array
		X_rolling_prediction_tmp = X_rolling_prediction_tmp.reshape(1, -1)
		# append forecast to the collection of forecasts
		Y_forecast = np.append(Y_forecast, [y_timestep_prediction_tmp], axis=0)

	return all_forecast_times, Y_forecast