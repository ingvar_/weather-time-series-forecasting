import unittest
from utils import convert_time_axis_to_datetime
from datetime import datetime

mock_timeaxis = ["2017-12-01 21:00", "2017-12-16 13:12"]
datetime_obj = []

for d in mock_timeaxis:
	datetime_obj.append(datetime.strptime(d, "%Y-%m-%d %H:%M"))


class ProjectTestCase(unittest.TestCase):
	# checks that the string timeaxis will be correctly converted to a datetime object
    def test_timeaxis_string_conversion_to_datetime_obj(self):
        result = convert_time_axis_to_datetime(mock_timeaxis)
        self.assertEqual(datetime_obj, result)