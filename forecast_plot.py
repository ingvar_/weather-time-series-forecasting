import datetime
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates



def generate_filename():
	''' Generate a filename for the output png file. '''
	timestamp = str(datetime.datetime.now())[0:16].replace(" ", "_")
	return "Forecast_" + timestamp + ".png"



def plot_and_save_forecast(timeaxis, Y_forecast, fname):
	''' Plot the generated temperature forecast using matplotlib and save it to pdf. '''
	fig = plt.figure()

	ax = fig.add_subplot(1, 1, 1)
	ax.xaxis.set_major_formatter(mdates.DateFormatter('%d/%m/%Y %H:%M'))
	ax.plot(timeaxis, np.ravel(Y_forecast))
	ax.set_ylabel('Temperature (C)')
	ax.grid()

	fig.autofmt_xdate(rotation=45)
	fig.tight_layout()
	fig.savefig(fname, facecolor=fig.get_facecolor())
	print("Figure " + fname + " saved.")

	return